﻿using RecommenderAPI.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Services
{
    public class SocialNetworkService
    {
        private readonly FacebookService _facebookService;
        private readonly GoogleService _googleService;
        private readonly FirebaseService _firebaseService;
        public SocialNetworkService(FacebookService facebookService, GoogleService googleService, FirebaseService firebaseService)
        {
            _facebookService = facebookService;
            _googleService = googleService;
            _firebaseService = firebaseService;
        }

        public async Task LoginUserAsync(string provider, string accessToken)
        {
            switch (provider)
            {
                case "Facebook":
                    var facebookUser = await _facebookService.GetUserInfo(accessToken);
                    var pictureUrl = await _facebookService.GetUserPicture(accessToken);
                    if (facebookUser == null)
                    {
                        throw new UnauthorizedAccessException();
                    }
                    await _firebaseService.LoginUser(provider, facebookUser.email, facebookUser.id, facebookUser.name, accessToken, pictureUrl);
                    break;
                case "Google":
                    var googleUser = await _googleService.GetUserInfo(accessToken);
                    if (googleUser == null)
                    {
                        throw new UnauthorizedAccessException();
                    }
                    await _firebaseService.LoginUser(provider, googleUser.email, googleUser.id, googleUser.name, accessToken, googleUser.picture);
                    break;
                default:
                    throw new NotImplementedException($"Social network provider '{provider}' not implemented");
            }
        }

        public async Task<SNUser> GetUser(string provider, string accessToken)
        {
            var user = await _firebaseService.GetUserDataByAccessToken(provider, accessToken);
            if(user != null)
            {
                return user;
            }
            string email = null;
            switch (provider)
            {
                case "Facebook":
                    var facebookUser = await _facebookService.GetUserInfo(accessToken);
                    email =  facebookUser?.email;
                    break;
                case "Google":
                    var googleUser = await _googleService.GetUserInfo(accessToken);
                    email = googleUser?.email;
                    break;
                default:
                    throw new NotImplementedException($"Social network provider '{provider}' not implemented");
            }
            if (email == null)
                throw new UnauthorizedAccessException();
            user = await _firebaseService.GetUserDataByEmail(provider, email);
            if (user == null)
                throw new UnauthorizedAccessException();
            user.AccessToken = $"{provider}@{accessToken}";
            var userId = await _firebaseService.GetUserId(provider, email);
            await _firebaseService.UpdateUser(user, userId);
            return user;
        }

        public async Task<string> GetUserEmail(string provider, string accessToken)
        {
            switch (provider)
            {
                case "Facebook":
                    var facebookUser = await _facebookService.GetUserInfo(accessToken);
                    return facebookUser.email;
                case "Google":
                    var googleUser = await _googleService.GetUserInfo(accessToken);
                    return googleUser.email;
                default:
                    throw new NotImplementedException($"Social network provider '{provider}' not implemented");
            }
        }

        public async Task<List<string>> GetSocialNetworkMovieTitles(string provider, string accessToken)
        {
            List<string> movieTitles = null;
            switch (provider)
            {
                case "Facebook":
                    movieTitles = await _facebookService.GetUserMoviesAsync(accessToken);
                    break;
                case "Google":
                    movieTitles = new List<string>();
                    break;
                default:
                    throw new NotImplementedException($"Social network provider '{provider}' not implemented");
            }
            return movieTitles;
        }

    }
}
