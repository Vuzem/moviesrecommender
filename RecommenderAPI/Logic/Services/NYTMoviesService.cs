﻿using RecommenderAPI.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;

namespace RecommenderAPI.Logic.Services
{
    public class NYTMoviesService
    {
        private readonly IHttpClientFactory _clientFactory;

        public NYTMoviesService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<string> GetReviewLink(string query)
        {
            using (var client = _clientFactory.CreateClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Get,
                $"https://api.nytimes.com/svc/movies/v2/reviews/search.json?query={HttpUtility.UrlEncode(query)}&api-key=bmWO31FZ7E4F5cJI6favz6NwxqzTiPp0");
                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    var reviews = await JsonSerializer.DeserializeAsync<ReviewList>(responseStream);
                    return reviews.results.Select(p=>p.link.url).FirstOrDefault();
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
