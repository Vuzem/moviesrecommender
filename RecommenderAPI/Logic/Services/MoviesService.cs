﻿using RecommenderAPI.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Services
{
    public class MoviesService
    {
        private readonly FirebaseService _firebaseService;
        private readonly SocialNetworkService _snService;
        public MoviesService(FirebaseService firebaseService, SocialNetworkService snService)
        {
            _firebaseService = firebaseService;
            _snService = snService;
        }

        public async Task<Movie> GetMovie(string provider, string accessToken, long id)
        {
            var movie = await _firebaseService.GetMovieById(id);
            var user = await _snService.GetUser(provider, accessToken);
            if (user == null)
                return movie;
            if (user.FavouriteMovies != null)
                movie.isFavourite = user.FavouriteMovies.Contains(id);
            return movie;
        }

        public async Task<Movie> GetMovie(long id)
        {
            var movie = await _firebaseService.GetMovieById(id);
            return movie;
        }

        public async Task<List<Movie>> GetUserMovies(string provider, string accessToken, int offset = 0, int limit = 10)
        {
            var movies = await _firebaseService.GetMoviesAsync(offset, limit);
            if (provider == null)
                return movies;
            var user = await _snService.GetUser(provider, accessToken);
            if (user == null) 
                return movies;
            if (user.FavouriteMovies != null)
                movies.ForEach(p => p.isFavourite = user.FavouriteMovies != null ? user.FavouriteMovies.Contains(p.id) : false);
            return movies;
        }

        public async Task<List<Movie>> SearchMovies(string query, string provider, string accessToken, int offset = 0, int limit = 10)
        {
            var movies = await _firebaseService.SearchMoviesAsync(query, offset, limit);
            if (provider == null)
                return movies;
            var user = await _snService.GetUser(provider, accessToken);
            if (user == null)
                return movies;
            if (user.FavouriteMovies != null)
                movies.ForEach(p => p.isFavourite = user.FavouriteMovies != null ? user.FavouriteMovies.Contains(p.id) : false);
            return movies;
        }

        public async Task<List<Movie>> GetFavouriteMovies(string provider, string accessToken, int offset = 0, int limit = 10)
        {
            var user = await _snService.GetUser(provider, accessToken);
            var favourites = new List<Movie>();
            if (user.FavouriteMovies != null)
            {
                foreach (var movieId in user.FavouriteMovies.Skip(offset).Take(limit))
                {
                    var temp = await GetMovie(movieId);
                    temp.isFavourite = true;
                    favourites.Add(temp);
                }
            }
            return favourites;
        }

        public async Task AddFavouriteMovie(string provider, string accessToken, long movieId)
        {
            var user = await _snService.GetUser(provider, accessToken);
            var userId = await _firebaseService.GetUserId(provider, user.Email);
            if (user.FavouriteMovies == null)
            {
                user.FavouriteMovies = new List<long>();
            }
            if (!user.FavouriteMovies.Contains(movieId))
            {
                user.FavouriteMovies.Add(movieId);
            }
            await _firebaseService.UpdateUser(user, userId);
        }

        public async Task DeleteFavouriteMovie(string provider, string accessToken, long movieId)
        {
            var user = await _snService.GetUser(provider, accessToken);
            var userId = await _firebaseService.GetUserId(provider, user.Email);
            if (user.FavouriteMovies != null)
            {
                user.FavouriteMovies.Remove(movieId);
                await _firebaseService.UpdateUser(user, userId);
            }
        }

        public async Task<List<string>> UpdateSocialNetworkMovies(string provider, string accessToken)
        {
            var movieTitles = await _snService.GetSocialNetworkMovieTitles(provider, accessToken);
            var user = await _snService.GetUser(provider, accessToken);
            if (user == null)
                throw new UnauthorizedAccessException();
            var id = await _firebaseService.GetUserId(provider, user.Email);
            user.SocialNetworkMovieTitles = movieTitles;
            var movieIds = new List<long>();
            foreach(var title in movieTitles)
            {
                var movie = await _firebaseService.GetMovieByTitle(title);
                if (movie != null)
                    movieIds.Add(movie.id);
            }
            user.SocialNetworkMovies = movieIds;
            await _firebaseService.UpdateUser(user, id);
            return movieTitles;
        }



        public async Task<List<Movie>> GetRecomendedMovies(string provider, string accessToken, int offset = 0, int limit = 8, int recommendationCount = 8)
        {
            var allFavourites = await _firebaseService.GetListOfFavouriteMovies();
            var user = await _snService.GetUser(provider, accessToken);
            var userFavourites = new List<long>();
            if (user.FavouriteMovies == null) user.FavouriteMovies = new List<long>();
            if (user.SocialNetworkMovies == null) user.SocialNetworkMovies = new List<long>();
            userFavourites.AddRange(user.FavouriteMovies);
            userFavourites.AddRange(user.SocialNetworkMovies.Where(p => !userFavourites.Contains(p)).ToList());
            var similarity = new Dictionary<int, int>();
            int i = 0;
            foreach(var favourites in allFavourites)
            {
                if(favourites == null)
                {
                    similarity.Add(i++, 0);
                    continue;
                }
                similarity.Add(i++, favourites.Where(p => userFavourites.Contains(p)).Count());
            }
            var indexes = similarity.AsEnumerable().Where(p=>p.Value>(userFavourites.Count/2)).Select(p => p.Key).ToList();
            if(indexes.Count <= 1) indexes = similarity.AsEnumerable().Select(p => p.Key).ToList();
            var recommendedMovies = new Dictionary<long,int>();
            foreach(var index in indexes)
            {
                var temp = allFavourites[index] != null ? allFavourites[index] : new List<long>();
                foreach(var movieId in temp)
                {
                    if (!userFavourites.Contains(movieId))
                    {
                        if (!recommendedMovies.ContainsKey(movieId))
                        {
                            recommendedMovies.Add(movieId, 1);
                        }
                        else
                        {
                            recommendedMovies[movieId] = recommendedMovies[movieId] + 1;
                        }
                    }
                }
            }
            var recommendedList = recommendedMovies.AsEnumerable().OrderByDescending(p=>p.Value).Take(recommendationCount).Select(p=>p.Key).ToList();
            user.RecomendedMovies = recommendedList;
            var id = await _firebaseService.GetUserId(provider, user.Email);
            await _firebaseService.UpdateUser(user, id);
            var recommendations = new List<Movie>();
            foreach (var movieId in recommendedList.Skip(offset).Take(limit))
            {
                recommendations.Add(await GetMovie(movieId));
            }
            return recommendations;
        }

        public async Task<List<Movie>> GetBestMovies(int offset = 0, int limit = 5, int recommendationCount = 10)
        {
            var allFavourites = await _firebaseService.GetListOfFavouriteMovies();
            var recommendedMovies = new Dictionary<long, int>();
            foreach (var tempFav in allFavourites)
            {
                var temp = tempFav != null ? tempFav : new List<long>();
                foreach (var movieId in temp)
                {
                    if (!recommendedMovies.ContainsKey(movieId))
                    {
                        recommendedMovies.Add(movieId, 1);
                    }
                    else
                    {
                        recommendedMovies[movieId] = recommendedMovies[movieId] + 1;
                    }
                }
            }
            var recommendedList = recommendedMovies.AsEnumerable().OrderByDescending(p => p.Value).Take(recommendationCount).Select(p => p.Key).ToList();
            var recommendations = new List<Movie>();
            foreach (var movieId in recommendedList.Skip(offset).Take(limit))
            {
                recommendations.Add(await GetMovie(movieId));
            }
            return recommendations;
        }
    }
}
