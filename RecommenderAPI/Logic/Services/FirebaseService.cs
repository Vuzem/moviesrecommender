﻿using Firebase.Database;
using Firebase.Database.Query;
using RecommenderAPI.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Services
{
    public class FirebaseService
    {
        public async Task<List<Movie>> GetMoviesAsync(int offset, int limit)
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var movies = await client.Child("movies").OrderByKey().StartAt(offset.ToString()).EndAt((offset + limit-1).ToString()).OnceAsync<Movie>();
            foreach(var movie in movies)
            {
                movie.Object.id = long.Parse(movie.Key);
            }
            return movies.Select(p => p.Object).ToList();
        }

        public async Task<List<Show>> GetShowsAsync(int offset, int limit)
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var shows = await client.Child("shows").OrderByKey().StartAt(offset.ToString()).EndAt((offset + limit - 1).ToString()).OnceAsync<Show>();
            foreach (var movie in shows)
            {
                movie.Object.id = long.Parse(movie.Key);
            }
            return shows.Select(p => p.Object).ToList();
        }

        public async Task<List<Movie>> SearchMoviesAsync(string query, int offset, int limit)
        {
            if (offset != 0)
            {
                return new List<Movie>();
            }
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var movies = await client.Child("movies").OrderBy("title").StartAt(query).LimitToFirst(limit).OnceAsync<Movie>();
            foreach (var movie in movies)
            {
                movie.Object.id = long.Parse(movie.Key);
            }
            return movies.Select(p => p.Object).ToList();
        }

        public async Task<List<Show>> SearchShowsAsync(string query, int offset, int limit)
        {
            if (offset != 0)
            {
                return new List<Show>();
            }
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var shows = await client.Child("shows").OrderBy("name").StartAt(query).LimitToFirst(limit).OnceAsync<Show>();
            foreach (var movie in shows)
            {
                movie.Object.id = long.Parse(movie.Key);
            }
            return shows.Select(p => p.Object).ToList();
        }

        public async Task LoginUser(string provider, string email, string id, string name, string accessToken, string picture)
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var users = await client.Child("users").OrderBy("Email").EqualTo(email).OnceAsync<SNUser>();
            if (!users.Where(p => p.Object.Provider == provider && p.Object.Email == email).Any())
            {
                var temp = new SNUser()
                {
                    Provider = provider,
                    Email = email,
                    Identifier = id,
                    Name = name,
                    Picture = picture,
                    AccessToken = $"{provider}@{accessToken}"
                };
                await client.Child("users").PostAsync(temp);
            }
            else
            {
                var user = users.Where(p => p.Object.Provider == provider && p.Object.Email == email).FirstOrDefault();
                user.Object.Name = name;
                user.Object.AccessToken = $"{provider}@{accessToken}";
                user.Object.Identifier = id;
                user.Object.Picture = picture;
                await client.Child("users").Child(user.Key).PatchAsync(user.Object);
            }
        }

        public async Task<SNUser> GetUserDataByEmail(string provider, string email)
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var users = await client.Child("users").OrderBy("Email").EqualTo(email).OnceAsync<SNUser>();
            return users.Where(p => p.Object.Provider == provider && p.Object.Email == email).Select(p=>p.Object).FirstOrDefault();
        }

        public async Task<SNUser> GetUserDataByAccessToken(string provider, string accessToken)
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var users = await client.Child("users").OrderBy("AccessToken").EqualTo($"{provider}@{accessToken}").OnceAsync<SNUser>();
            return users.Select(p => p.Object).FirstOrDefault();
        }

        public async Task<string> GetUserId(string provider, string email)
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var users = await client.Child("users").OrderBy("Email").EqualTo(email).OnceAsync<SNUser>();
            return users.Where(p => p.Object.Provider == provider && p.Object.Email == email).Select(p => p.Key).FirstOrDefault();
        }

        public async Task UpdateUser(SNUser data, string id)
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            await client.Child("users").Child(id).PatchAsync(data);
        }

        public async Task<List<List<long>>> GetListOfFavouriteMovies()
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var users = await client.Child("users").OnceAsync<SNUser>();
            return users.Select(p => 
            {
                var favourites = new List<long>();
                if (p.Object.FavouriteMovies != null) favourites.AddRange(p.Object.FavouriteMovies);
                if (p.Object.SocialNetworkMovies != null) favourites.AddRange(p.Object.SocialNetworkMovies);
                return favourites;
            }).ToList();
        }

        public async Task<List<List<long>>> GetListOfFavouriteShows()
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var users = await client.Child("users").OnceAsync<SNUser>();
            return users.Select(p => p.Object.FavouriteShows).ToList();
        }

        public async Task<Movie> GetMovieById(long id)
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var movie = await client.Child("movies").Child(id.ToString()).OnceSingleAsync<Movie>();
            movie.id = id;
            return movie;
        }

        public async Task<Show> GetShowById(long id)
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var show = await client.Child("shows").Child(id.ToString()).OnceSingleAsync<Show>();
            show.id = id;
            return show;
        }

        public async Task<Movie> GetMovieByTitle(string title)
        {
            var client = new FirebaseClient("https://dmlab-79d5b.firebaseio.com");
            var movies = await client.Child("movies").OrderBy("title").EqualTo(title).OnceAsync<Movie>();
            foreach(var movie in movies)
            {
                movie.Object.id = long.Parse(movie.Key);
            }
            return movies.Select(p=>p.Object).FirstOrDefault();
        }

    }
}
