﻿using RecommenderAPI.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Services
{
    public class ShowsService
    {
        private readonly FirebaseService _firebaseService;
        private readonly SocialNetworkService _snService;
        public ShowsService(FirebaseService firebaseService, SocialNetworkService snService)
        {
            _firebaseService = firebaseService;
            _snService = snService;
        }

        public async Task<Show> GetShow(string provider, string accessToken, long id)
        {
            var show = await _firebaseService.GetShowById(id);
            var user = await _snService.GetUser(provider, accessToken);
            if (user == null)
                return show;
            if (user.FavouriteShows != null)
                show.isFavourite = user.FavouriteShows.Contains(id);
            return show;
        }

        public async Task<Show> GetShow(long id)
        {
            var show = await _firebaseService.GetShowById(id);
            return show;
        }

        public async Task<List<Show>> GetUserShows(string provider, string accessToken, int offset = 0, int limit = 10)
        {
            var shows = await _firebaseService.GetShowsAsync(offset, limit);
            if (provider == null)
                return shows;
            var user = await _snService.GetUser(provider, accessToken);
            if (user == null)
                return shows;
            if (user.FavouriteShows != null)
                shows.ForEach(p => p.isFavourite = user.FavouriteShows != null ? user.FavouriteShows.Contains(p.id) : false);
            return shows;
        }

        public async Task<List<Show>> SearchShows(string query, string provider, string accessToken, int offset = 0, int limit = 10)
        {
            var shows = await _firebaseService.SearchShowsAsync(query, offset, limit);
            if (provider == null)
                return shows;
            var user = await _snService.GetUser(provider, accessToken);
            if (user == null)
                return shows;
            if (user.FavouriteShows != null)
                shows.ForEach(p => p.isFavourite = user.FavouriteShows != null ? user.FavouriteShows.Contains(p.id) : false);
            return shows;
        }

        public async Task<List<Show>> GetFavouriteShow(string provider, string accessToken, int offset = 0, int limit = 10)
        {
            var user = await _snService.GetUser(provider, accessToken);
            var favourites = new List<Show>();
            if (user.FavouriteShows != null)
            {
                foreach (var showId in user.FavouriteShows.Skip(offset).Take(limit))
                {
                    var temp = await GetShow(showId);
                    temp.isFavourite = true;
                    favourites.Add(temp);
                }
            }
            return favourites;
        }

        public async Task AddFavouriteShow(string provider, string accessToken, long ShowId)
        {
            var user = await _snService.GetUser(provider, accessToken);
            var userId = await _firebaseService.GetUserId(provider, user.Email);
            if (user.FavouriteShows == null)
            {
                user.FavouriteShows = new List<long>();
            }
            if (!user.FavouriteShows.Contains(ShowId))
            {
                user.FavouriteShows.Add(ShowId);
            }
            await _firebaseService.UpdateUser(user, userId);
        }

        public async Task DeleteFavouriteShow(string provider, string accessToken, long ShowId)
        {
            var user = await _snService.GetUser(provider, accessToken);
            var userId = await _firebaseService.GetUserId(provider, user.Email);
            if (user.FavouriteShows != null)
            {
                user.FavouriteShows.Remove(ShowId);
                await _firebaseService.UpdateUser(user, userId);
            }
        }

        public async Task<List<Show>> GetRecomendedShows(string provider, string accessToken, int offset = 0, int limit = 8, int recommendationCount = 8)
        {
            var allFavourites = await _firebaseService.GetListOfFavouriteShows();
            var user = await _snService.GetUser(provider, accessToken);
            if (user.FavouriteShows == null) user.FavouriteShows = new List<long>();
            var userFavourites = new List<long>(user.FavouriteShows);
            var similarity = new Dictionary<int, int>();
            int i = 0;
            foreach (var favourites in allFavourites)
            {
                if (favourites == null)
                {
                    similarity.Add(i++, 0);
                    continue;
                }
                similarity.Add(i++, favourites.Where(p => userFavourites.Contains(p)).Count());
            }
            var indexes = similarity.AsEnumerable().Where(p => p.Value > (userFavourites.Count / 2)).Select(p => p.Key).ToList();
            if (indexes.Count <= 1) indexes = similarity.AsEnumerable().Select(p => p.Key).ToList();
            var recommendedShows = new Dictionary<long, int>();
            foreach (var index in indexes)
            {
                var temp = allFavourites[index] != null ? allFavourites[index] : new List<long>();
                foreach (var showId in temp)
                {
                    if (!userFavourites.Contains(showId))
                    {
                        if (!recommendedShows.ContainsKey(showId))
                        {
                            recommendedShows.Add(showId, 1);
                        }
                        else
                        {
                            recommendedShows[showId] = recommendedShows[showId] + 1;
                        }
                    }
                }
            }
            var recommendedList = recommendedShows.AsEnumerable().OrderByDescending(p => p.Value).Take(recommendationCount).Select(p => p.Key).ToList();
            user.RecomendedShows = recommendedList;
            var id = await _firebaseService.GetUserId(provider, user.Email);
            await _firebaseService.UpdateUser(user, id);
            var recommendations = new List<Show>();
            foreach (var showId in recommendedList.Skip(offset).Take(limit))
            {
                recommendations.Add(await GetShow(showId));
            }
            return recommendations;
        }

        public async Task<List<Show>> GetBestShows(int offset = 0, int limit = 5, int recommendationCount = 10)
        {
            var allFavourites = await _firebaseService.GetListOfFavouriteShows();
            var recommendedShows = new Dictionary<long, int>();
            foreach (var tempFav in allFavourites)
            {
                var temp = tempFav != null ? tempFav : new List<long>();
                foreach (var showId in temp)
                {
                    if (!recommendedShows.ContainsKey(showId))
                    {
                        recommendedShows.Add(showId, 1);
                    }
                    else
                    {
                        recommendedShows[showId] = recommendedShows[showId] + 1;
                    }
                }
            }
            var recommendedList = recommendedShows.AsEnumerable().OrderByDescending(p => p.Value).Take(recommendationCount).Select(p => p.Key).ToList();
            var recommendations = new List<Show>();
            foreach (var movieId in recommendedList.Skip(offset).Take(limit))
            {
                recommendations.Add(await GetShow(movieId));
            }
            return recommendations;
        }
    }
}
