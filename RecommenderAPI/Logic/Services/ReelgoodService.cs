﻿using RecommenderAPI.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Services
{
    public class ReelgoodService
    {
        private readonly IHttpClientFactory _clientFactory;

        public ReelgoodService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<Movie> GetRandomMovie()
        {
            using (var client = _clientFactory.CreateClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Get,
                "https://api.reelgood.com/roulette/netflix?availability=onAnySource&content_kind=movie&nocache=true");
                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    var result = await JsonSerializer.DeserializeAsync<ReelgoodModel>(responseStream);
                    return new Movie() {
                        title = result.title,
                        overview = result.overview,
                        release_date = result.released_on,
                        vote_average = result.imdb_rating,
                        poster_path = $"https://img.reelgood.com/content/movie/{result.id}/poster-780.webp"
                    };
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task<Show> GetRandomShow()
        {
            using (var client = _clientFactory.CreateClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Get,
                "https://api.reelgood.com/roulette/netflix?availability=onAnySource&content_kind=movie&nocache=true");
                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    var result = await JsonSerializer.DeserializeAsync<ReelgoodModel>(responseStream);
                    return new Show()
                    {
                        name = result.title,
                        summary = result.overview,
                        rating = new ShowRating() {
                            average = result.imdb_rating
                        },
                        image = new ShowImage(){
                            original = $"https://img.reelgood.com/content/movie/{result.id}/poster-780.webp",
                            medium = $"https://img.reelgood.com/content/movie/{result.id}/poster-92.webp"
                        }
                    };
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
