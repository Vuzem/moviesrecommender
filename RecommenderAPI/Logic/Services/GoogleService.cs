﻿using RecommenderAPI.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Services
{
    public class GoogleService
    {
        private readonly IHttpClientFactory _clientFactory;

        public GoogleService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<GoogleUser> GetUserInfo(string accessToken)
        {
            using (var client = _clientFactory.CreateClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Get,
                "https://oauth2.googleapis.com/tokeninfo?id_token=" + accessToken);
                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    var user = await JsonSerializer.DeserializeAsync<GoogleUser>(responseStream);
                    return user;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
