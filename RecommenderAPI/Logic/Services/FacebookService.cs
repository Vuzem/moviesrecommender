﻿using RecommenderAPI.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Services
{
    public class FacebookService
    {
        private readonly IHttpClientFactory _clientFactory;

        public FacebookService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }

        public async Task<FacebookUser> GetUserInfo(string accessToken)
        {
            using (var client = _clientFactory.CreateClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Get,
                "https://graph.facebook.com/v5.0/me?fields=email%2Cid%2Cname&access_token=" + accessToken);
                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    var user = await JsonSerializer.DeserializeAsync<FacebookUser>(responseStream);
                    return user;
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task<string> GetUserPicture(string accessToken)
        {
            using (var client = _clientFactory.CreateClient())
            {
                var request = new HttpRequestMessage(HttpMethod.Get,
                "https://graph.facebook.com/v5.0/me/picture?height=480&redirect&access_token=" + accessToken);
                var response = await client.SendAsync(request);

                if (response.IsSuccessStatusCode)
                {
                    using var responseStream = await response.Content.ReadAsStreamAsync();
                    var picture = await JsonSerializer.DeserializeAsync<FacebookPicture>(responseStream);
                    return picture.data.url;
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task<List<string>> GetUserMoviesAsync(string accessToken)
        {
            using (var client = _clientFactory.CreateClient())
            {
                List<string> resultTitles = new List<string>();
                string nextPageUrl = "https://graph.facebook.com/v5.0/me/movies?fields=name&limit=25&access_token=" + accessToken;
                while (nextPageUrl != null)
                {
                    var request = new HttpRequestMessage(HttpMethod.Get, nextPageUrl);
                    var response = await client.SendAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        using var responseStream = await response.Content.ReadAsStreamAsync();
                        var result = await JsonSerializer.DeserializeAsync<FacebookMovies>(responseStream);
                        resultTitles.AddRange(result.data.Select(p=>p.name).ToList());
                        nextPageUrl = result.paging.next;
                    }
                    else
                    {
                        break;
                    }
                }
                return resultTitles;
            }
        }
    }
}
