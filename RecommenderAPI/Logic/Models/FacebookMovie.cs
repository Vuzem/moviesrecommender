﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Models
{
    public class FacebookMovies
    {
        public List<FacebookMovie> data { get; set; }
        public Paging paging { get; set; }
    }
    public class FacebookMovie
    {
        public string name { get; set; }
    }
    public class Paging
    {
        public string next { get; set; }
        public string previous { get; set; }
    }

}
