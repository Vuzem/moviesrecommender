﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Models
{
    public class Movie
    {
        public long id { get; set; }
        public string imdb_id { get; set; }
        public string title { get; set; }
        public string overview { get; set; }
        public double vote_average { get; set; }
        public double popularity { get; set; }
        public string poster_path { get; set; }
        public string backdrop_path { get; set; }
        public string original_language { get; set; }
        public List<Genre> genres { get; set; }
        public DateTime? release_date { get; set; }
        public bool isFavourite { get; set; }
        public string nyt_review { get; set; }
    }

}
