﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Models
{
    public class ShowRating
    {
        public double average { get; set; }
    }
}
