﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Models
{
    public class SNUser
    {
        public string Provider { get; set; }
        public string Email { get; set; }
        public string AccessToken { get; set; }
        public string Identifier { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public List<long> FavouriteMovies { get; set; }
        public List<long> FavouriteShows { get; set; }
        public List<long> RecomendedMovies { get; set; }
        public List<long> RecomendedShows { get; set; }
        public List<long> SocialNetworkMovies { get; set; }
        public List<string> SocialNetworkMovieTitles { get; set; }
    }
}
