﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Models
{
    public class ShowImage
    {
        public string medium { get; set; }
        public string original { get; set; }
    }
}
