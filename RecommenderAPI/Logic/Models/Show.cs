﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Models
{
    public class Show
    {
        public long id { get; set; }
        public string officialSite { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string language { get; set; }
        public List<string> genres { get; set; }
        public ShowRating rating { get; set; }
        public ShowImage image { get; set; }
        public string summary { get; set; }
        public bool isFavourite { get; set; }
    }

}
