﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Models
{
    public class Genre
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}