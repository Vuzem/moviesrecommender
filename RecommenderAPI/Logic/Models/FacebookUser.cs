﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Models
{
    public class FacebookUser
    {
        public string email { get; set; }
        public string id { get; set; }
        public string name { get; set; }
    }

    public class FacebookPicture
    {
        public FacebookPictureData data { get; set; }
    }

    public class FacebookPictureData
    {
        public string url { get; set; }
    }
}
