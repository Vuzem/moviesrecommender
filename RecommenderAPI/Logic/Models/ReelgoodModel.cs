﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Models
{
    public class ReelgoodModel
    {
        public string id { get; set; }
        public string title { get; set; }
        public string overview { get; set; }
        public DateTime released_on { get; set; }
        public double imdb_rating { get; set; }
    }
}
