﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecommenderAPI.Logic.Models
{
    public class NYTReview
    {
        public string display_title { get; set; }
        public ReviewLink link { get; set; }
    }

    public class ReviewLink
    {
        public string type { get; set; }
        public string url { get; set; }
        public string suggested_link_text { get; set; }
    }

    public class ReviewList
    {
        public int num_results { get; set; }
        public List<NYTReview> results { get; set; }

    }
}
