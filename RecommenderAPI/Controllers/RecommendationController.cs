﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RecommenderAPI.Logic.Models;
using RecommenderAPI.Logic.Services;

namespace RecommenderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecommendationController : ControllerBase
    {
        private readonly MoviesService _moviesService;
        private readonly ShowsService _showsService;
        private readonly ReelgoodService _reelgoodService;
        public RecommendationController(MoviesService moviesService, ShowsService showsService, ReelgoodService reelgoodService)
        {
            _moviesService = moviesService;
            _showsService = showsService;
            _reelgoodService = reelgoodService;
        }

        [HttpGet("Movies")]
        public async Task<ActionResult<List<Movie>>> GetRecomendedMoviesAsync(string provider, string accessToken, int offset = 0, int limit = 8)
        {
            try
            {
                return Ok(await _moviesService.GetRecomendedMovies(provider, accessToken, offset, limit));
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpGet("Shows")]
        public async Task<ActionResult<List<Show>>> GetRecomendedShowsAsync(string provider, string accessToken, int offset = 0, int limit = 8)
        {
            try 
            {
                return Ok(await _showsService.GetRecomendedShows(provider, accessToken, offset, limit));
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpGet("Movies/Random")]
        public async Task<ActionResult<Movie>> GetRandomMovieAsync()
        {
            return Ok(await _reelgoodService.GetRandomMovie());
        }

        [HttpGet("Shows/Random")]
        public async Task<ActionResult<Show>> GetRandomShowAsync()
        {
            return Ok(await _reelgoodService.GetRandomShow());
        }

        [HttpGet("Movies/Best")]
        public async Task<ActionResult<List<Movie>>> GetBestMoviesAsync(int offset = 0, int limit = 10)
        {
            return Ok(await _moviesService.GetBestMovies(offset, limit));
        }

        [HttpGet("Shows/Best")]
        public async Task<ActionResult<List<Show>>> GetBestShowsAsync(int offset = 0, int limit = 10)
        {
            return Ok(await _showsService.GetBestShows(offset, limit));
        }
    }
}