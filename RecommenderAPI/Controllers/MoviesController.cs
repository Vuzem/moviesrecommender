﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RecommenderAPI.Logic.Models;
using RecommenderAPI.Logic.Services;

namespace RecommenderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MoviesService _moviesService;
        private readonly NYTMoviesService _nytService;
        public MoviesController(MoviesService moviesService, NYTMoviesService nytService)
        {
            _moviesService = moviesService;
            _nytService = nytService;
        }

        [HttpGet]
        public async Task<ActionResult<List<Movie>>> GetMoviesAsync(string provider, string accessToken, int offset=0, int limit=10)
        {
            try
            {
                var movies = await _moviesService.GetUserMovies(provider, accessToken, offset, limit);
                //foreach (var movie in movies)
                //{
                //    movie.nyt_review = await _nytService.GetReviewLink(movie.title);
                //}
                return Ok(movies);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Movie>> GetMovieAsync(string provider, string accessToken, long id)
        {
            try
            {
                var movie = await _moviesService.GetMovie(provider, accessToken, id);
                movie.nyt_review = await _nytService.GetReviewLink(movie.title);
                return Ok(movie);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpGet("Search")]
        public async Task<ActionResult<List<Movie>>> GetMoviesSearch(string query, string provider, string accessToken, int offset = 0, int limit = 10)
        {
            try
            {
                var movies = await _moviesService.SearchMovies(query, provider, accessToken, offset, limit);
                foreach (var movie in movies)
                {
                    movie.nyt_review = await _nytService.GetReviewLink(movie.title);
                }
                return Ok(movies);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }
    }
}