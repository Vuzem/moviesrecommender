﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Firebase.Database;
using Firebase.Database.Query;
using RecommenderAPI.Logic.Models;
using RecommenderAPI.Logic.Services;

namespace RecommenderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly SocialNetworkService _snService;
        private readonly MoviesService _moviesService;
        public UserController(SocialNetworkService socialNetworkService, MoviesService moviesService)
        {
            _snService = socialNetworkService;
            _moviesService = moviesService;
        }
        [HttpPost("Login")]
        public async Task<IActionResult> LoginAsync(string provider, string accessToken)
        {
            try
            {
                await _snService.LoginUserAsync(provider, accessToken);
                await _moviesService.UpdateSocialNetworkMovies(provider, accessToken);
                return Ok();
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }


        [HttpGet]
        public async Task<ActionResult<SNUser>> GetInfoAsync(string provider, string accessToken)
        {
            try
            {
                return Ok(await _snService.GetUser(provider, accessToken));
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpGet("LikedMovies")]
        public async Task<ActionResult<List<string>>> GetMoviesAsync(string provider, string accessToken)
        {
            try
            {
                var movieTitles = await _moviesService.UpdateSocialNetworkMovies(provider, accessToken);
                
                return Ok(movieTitles);
            }
            catch
            {
                return Unauthorized();
            }
        }
    }
}