﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RecommenderAPI.Logic.Models;
using RecommenderAPI.Logic.Services;

namespace RecommenderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShowsController : ControllerBase
    {
        private readonly ShowsService _showsSevice;
        public ShowsController(ShowsService showsService)
        {
            _showsSevice = showsService;
        }

        [HttpGet]
        public async Task<ActionResult<List<Show>>> GetShowsAsync(string provider, string accessToken, int offset = 0, int limit = 10)
        {
            try
            {
                return Ok(await _showsSevice.GetUserShows(provider, accessToken, offset, limit));
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Show>> GetShowAsync(string provider, string accessToken, long id)
        {
            try
            {
                var show = await _showsSevice.GetShow(provider, accessToken, id);
                return Ok(show);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpGet("Search")]
        public async Task<ActionResult<List<Show>>> GetShowsSearch(string query, string provider, string accessToken, int offset = 0, int limit = 10)
        {
            try
            {
                return Ok(await _showsSevice.SearchShows(query, provider, accessToken, offset, limit));
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }
    }
}