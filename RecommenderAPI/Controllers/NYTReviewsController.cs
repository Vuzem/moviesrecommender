﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RecommenderAPI.Logic.Services;

namespace RecommenderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NYTReviewsController : ControllerBase
    {
        private readonly NYTMoviesService _nytService;
        public NYTReviewsController(NYTMoviesService nytService)
        {
            _nytService = nytService;
        }

        [HttpGet]
        public async Task<string> GetReview(string query)
        {
            return await _nytService.GetReviewLink(query);
        }
    }
}