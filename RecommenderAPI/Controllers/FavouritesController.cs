﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RecommenderAPI.Logic.Models;
using RecommenderAPI.Logic.Services;

namespace RecommenderAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FavouritesController : ControllerBase
    {
        private readonly MoviesService _moviesService;
        private readonly ShowsService _showsService;
        private readonly NYTMoviesService _nytService;

        public FavouritesController(MoviesService moviesService, ShowsService showsService, NYTMoviesService nytService)
        {
            _moviesService = moviesService;
            _showsService = showsService;
            _nytService = nytService;
        }

        [HttpGet("Movie")]
        public async Task<IActionResult> GetFavouriteMovies(string provider, string accessToken, int offset = 0, int limit = 10)
        {
            try
            {
                var movies = await _moviesService.GetFavouriteMovies(provider, accessToken, offset, limit);
                foreach (var movie in movies)
                {
                    movie.nyt_review = await _nytService.GetReviewLink(movie.title);
                }
                return Ok(movies);
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpPut("Movie")]
        public async Task<IActionResult> AddFavouriteMovieAsync(string provider, string accessToken, long movieId)
        {
            try
            {
                await _moviesService.AddFavouriteMovie(provider, accessToken, movieId);
                return Ok();
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpDelete("Movie")]
        public async Task<IActionResult> DeleteFavouriteMovieAsync(string provider, string accessToken, long movieId)
        {
            try
            {
                await _moviesService.DeleteFavouriteMovie(provider, accessToken, movieId);
                return Ok();
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpGet("Show")]
        public async Task<IActionResult> GetFavouriteShows(string provider, string accessToken, int offset = 0, int limit = 10)
        {
            try
            {
                return Ok(await _showsService.GetFavouriteShow(provider, accessToken, offset, limit));
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpPut("Show")]
        public async Task<IActionResult> AddFavouriteShowAsync(string provider, string accessToken, long showId)
        {
            try
            {
                await _showsService.AddFavouriteShow(provider, accessToken, showId);
                return Ok();
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }

        [HttpDelete("Show")]
        public async Task<IActionResult> DeleteFavouriteShowAsync(string provider, string accessToken, long showId)
        {
            try
            {
                await _showsService.DeleteFavouriteShow(provider, accessToken, showId);
                return Ok();
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }
        }
    }
}